# PHP & Apache image for Drupal

## Introduction

A Docker image for running Drupal as the web server or CLI container.
The image also comes with script functionality to make altering PHP, Apache or
additional services easy.

Examples uses for these functions:
  * Add VHost to Apache
  * Alter **PHP** configurations
  * installing and configuring **Redis**.

The image uses *s6* to manage running processes. Adding your startup scripts
to the `/etc/s6` folder will run those scripts on container start-up
(_see section on startup processes_).

See section on scripted functions for more information about making use of
these scripts to create derivative images.


## Alpine Linux

This image uses Alpine Linux, and allows for the installation of additional
Alpine supported packages to be installed using the *APK* pakage manager.

List of supported packages is (here)[https://pkgs.alpinelinux.org/packages]
and can be installed using the command:

```

# These "--no-cache" is recommended on a Docker image to avoid extra bloat.
apk add --update --no-cache list of package names

```

Note that, if you are installing a service this way, you will need to ensure
that it gets started correctly. See the next section on *Startup Processes*
for more information on how this container is setup startup and manage
multiple processes / daemons.


## Startup Processes (*s6*)

This Docker image uses *(s6)[http://skarnet.org/software/s6/]* as the service
supervision daemon. It is light-weight and works simply by using shell scripts
placed in the `/etc/s6` folder. To add a service add a folder with `run` and
`finish` scripts for the process you'd like to add run during the container
startup.

This docker image includes a *s6* folder which gets copied into the `/root`
directory. This directory contains all startup settings for all pre-configured
services and processes I have already prepared, and each of these folders get
copied into the `/etc` folder as the setup script is used to enable the service.

The script files need to have the _execute_ permission in order to be run, and
the `run` script should activate your service using the shell `exec` command. If
you want this service enabled, you may directly place your daemon startup
scripts into the `/etc/s6` folder.

*Note:* SSH is purposely not enabled in this build. This package should be
setup and installed on the local machine, so it will generate the unique SSH
keys for each environment. Run `/scripts/setup.sh configure Sshd` in your
_Dockerfile_.


## Scripted Functions (*setup.sh*)

The image includes a `scripts/setup.sh` file which is added to the Docker image
and available for use in derivative `Dockerfile` to install services, alter
configurations and setup VHosts.

*An example of how these might be used in a Dockerfile:*
```
FROM tinkersmith/drupal:php7-dev

ARG PROJECT_DOMAIN="local.example.com"

# NOTE: ** SSH was purposely delayed to be built on the fly so the SSH
# generated keys will be unique to the container that builds it.
RUN /scripts/setup.sh configureSshd && \
  /scripts/setup.sh configureRedis && \
  /scripts/setup.sh setHostname ${PROJECT_DOMAIN} && \
  /scripts/setup.sh setPhpConfig 'post_max_size' '200M' && \
  /scripts/setup.sh setRedisConfig "maxmemory" "128mb"
```


*Available functions with full descriptions are listed below:*

### Configuration Helper functions

*`function setIniConfig`*

Creates or sets option in INI file format. Is able to replace options of the
matching name that is commented out.

+ _param $1_ - Name of the configuration to modify.
+ _param $2_ - The value to set this configuration to.
+ _param $3_ - The file to set the configuration in (full path only).

*`function setPhpConfig`*

Sets options for PHP INI files. Main difference from the `setIniConfig` function
is that it will default to setting the options in the `php.ini` file, and will
know to look inside the PHP `config.d` for other settings files specified by the
third parameter.

+ _param $1_ - Name of the configuration to modify.
+ _param $2_ - The value to set this configuration to.
+ _param $3_ - The file to set the configuration in (full path, or extension name) defaults to `php.ini`.

*`function setRedisConfig`*

Alter a Redis configuration. This build only uses a single configurations file located at `/etc/redis.conf`.

+ _param $1_ - The name of the configuration to modify.
+ _param $2_ - The value to set the configuration to.

### Apache helper functions

*`function enableApacheMod`*

Enable an Apache web server module. Will only enable modules that are available
on the server and listed in the `httpd.conf` file included in this repo.

+ _param $1_ - Name of the Apache2 module to enable.

*`function disableApacheMod`*

Disables an Apache web server module. Only disables modules in the active
`httpd.conf` file.

+ _param $1_ - Name of the Apache2 module to disable.

*`function createVHost`*

Creates a VHost entry for Apache from the VHost template.

+ _param $1_ - The Docroot of the VHost (should be relative to /var/www/).
+ _param $2_ - The domain to use for the VHost
+ _param $3_ - The abbreviated or short name for the site. (OPTIONAL)

*`function setHostname`*

Set the Apache hostname (ServerName) setting and is separated from the regular
setup because this will usually get set per project, and will want to happen
outside Docker images pushed to Docker hub.

+ _param $1_ - Domain to use as the default Apache web server domain.

### Service install and configurations

*`function configureSshd`*

Install and setup SSHD daemon. Should be run on each individual environments
so that SSHD will have unique SSH keys and signatures for each instance.
S6 start-up scripts will also be installed so that the SSHD will be active
started with the container, and will check for public keys in the /root/.ssh
folder to add for access.

*`function configureRedis`*

Install and setup the Redis server with default options for a local environment.
This function will also configure the S6 scripts, so that Redis will run on
container start-up.


#### NOTE: Why no autobuild?

At the time of this writing, Docker Hub does not support the --squash option
when it builds images and until it does, I will resort to a using a scripted
build and push process.

Keeping the images small is important to me and worth the extra effort to
control the build and deployment process.

Use the included ./build.sh script to rebuilt this container. The script does
contain a couple environment variables that can be changed to adjust how
the built container is tagged and makes use of some internal container options.
