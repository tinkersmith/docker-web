FROM alpine:3.19

ENV DEV_ENABLED 1

ENV PHP_VER 82
ENV PHP_INI_DIR "/etc/php${PHP_VER}"

# Install the required packages
RUN apk add --update --no-cache \
  s6 sudo bash ncurses curl sed rsync pcre patch git \
  mariadb-client libsodium openssl openssh-client \
  composer nodejs npm yarn \
  php${PHP_VER} \
  php${PHP_VER}-opcache \
  php${PHP_VER}-fileinfo \
  php${PHP_VER}-intl \
  php${PHP_VER}-openssl \
  php${PHP_VER}-pdo_mysql \
  php${PHP_VER}-pdo_sqlite \
  php${PHP_VER}-ctype \
  php${PHP_VER}-session \
  php${PHP_VER}-curl \
  php${PHP_VER}-gd \
  php${PHP_VER}-sodium \
  php${PHP_VER}-mbstring \
  php${PHP_VER}-dom \
  php${PHP_VER}-xml \
  php${PHP_VER}-xmlreader \
  php${PHP_VER}-xmlwriter \
  php${PHP_VER}-simplexml \
  php${PHP_VER}-phar \
  php${PHP_VER}-zip \
  php${PHP_VER}-iconv \
  php${PHP_VER}-pcntl \
  php${PHP_VER}-json \
  php${PHP_VER}-posix \
  php${PHP_VER}-tokenizer \
  php${PHP_VER}-exif \
  php${PHP_VER}-pecl-uploadprogress \
  php${PHP_VER}-pecl-apcu \
  php${PHP_VER}-pecl-redis \
  php${PHP_VER}-pecl-yaml \
  php${PHP_VER}-fpm

ADD s6 /root/s6
ADD setup.sh /scripts/setup.sh
ADD bin/config /usr/sbin/config

# Add a Drush user on the system (should SSH to this account instead of root).
RUN addgroup -g 1000 -S drush && \
  adduser -u 1000 -s /bin/bash -S -g drush drush && \
  echo "export PATH=\"\$HOME/.composer/vendor/bin:\$PATH\"" > /home/drush/.bashrc

# Copy configuration files.
RUN /scripts/setup.sh setup s6 && \
  /scripts/setup.sh setup php && \
  /scripts/setup.sh setup drush_launcher && \
  cp -a /root/s6/stayalive /etc/s6/stayalive

# Initialize the working directory to the base of the default web directory.
WORKDIR /project

# Start the s6 supervisor.
ENTRYPOINT ["s6-svscan","/etc/s6"]
