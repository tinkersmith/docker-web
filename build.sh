#!/bin/bash
#
# Script to build and push a new version of this Docker image.
#

# TODO: Add simple build options to allow building of different environments.
PHP_VERSION="8.2"

TAG="php${PHP_VERSION}"

function main {
  docker buildx build \
    --build-arg "DEVELOPMENT_TOOLS=1" \
    --file "Dockerfiles/apache.dockerfile" \
    -t "tinkersmith/apache:${TAG}-dev" \
    ./

  docker buildx build \
    --file "Dockerfiles/cli.dockerfile" \
    -t "tinkersmith/cli:${TAG}" \
    ./
}

main "$@"
