#!/bin/bash
#
# Helper setup and installation for an Apache/PHP server.
# ----------------------------------------------------------

function main {
  local cmd=${1}

  # Check if the command is a nicely formatted version of the command / function.
  case $cmd in
    setup|configure)
      cmd="setup_${2}"
      shift
      ;;
  esac

  # Move the current 1st argument out.
  shift

  if [[ $(type -t $cmd) == "function" ]]; then
    ${cmd} "$@"
  else
    echo "Unable to find the ${cmd} with arguments '$@'."
  fi
}


## ---------------------------
## Command definitions
## ---------------------------

#
# Initialize the s6 system services.
#
# This should typically be the first service setup for all these servers.
#
function setup_s6 {
  # Copy the s6 startup files.
  mkdir -p /etc/s6
  cp -a /root/s6/.s6-svscan/ /etc/s6/.s6-svscan
}

#
# Set the Apache hostname (ServerName) setting.
#
# Separated from the regular setup because this will usually get set
# per project, and will want to happen outside Docker images pushed
# to Docker hub.
#
function setHostname {
  local domain="${1}"
  local docroot="/var/www/html/${2:-"localhost"}"

  # Ensure that the docroot exists.
  mkdir -p "${docroot}"

  cat << EOF > "/etc/apache2/conf.d/site-domain.conf"
ServerName ${domain}:80

#
# DocumentRoot: The directory out of which you will serve your
# documents. By default, all requests are taken from this directory, but
# symbolic links and aliases may be used to point to other locations.
#
DocumentRoot "${docroot}"

<Directory "${docroot}">
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
EOF
}

#
# Setup PHP INI files to our requirements.
#
function setup_php {
  config php --multi \
    'expose_php' 'off' \
    'realpath_cache_size' '1M' \
    'realpath_cache_ttl' '3600' \
    'upload_max_filesize' '48M' \
    'post_max_size' '48M' \
    'memory_limit' '128M' \
    'max_input_vars' '3000' \
    'zend.assertions' '-1' \
    'assert.exception' 'Off'

  # Configure OpCache settings.
  config php --file=opcache --multi \
    'opcache.enable' '1' \
    'opcache.enable_cli' '0' \
    'opcache.memory_consumption' '128' \
    'opcache.interned_strings_buffer' '8' \
    'opcache.max_accelerated_files' '16229' \
    'opcache.revalidate_freq' '61' \
    'opcache.validate_timestamps' 'On' \
    'opcache.fast_shutdown' '1' \
    'opcache.jit_buffer_size' '64M' \
    'opcache.jit' '1255'

  # If there is no PHP CLI command it could be named `php{PHP_VER}`
  # and if it is, then create a simple symbolic link to it.
  if ! [ -f /usr/bin/php ] && [ -f /usr/bin/php${PHP_VER} ]; then
    ln -s /usr/bin/php${PHP_VER} /usr/bin/php;
  fi

  # Ensure that the CLI has enough memory to run composer updates.
  cp "${PHP_INI_DIR}/php.ini" "${PHP_INI_DIR}/php-cli.ini"
  config php "memory_limit" "1536M" "${PHP_INI_DIR}/php-cli.ini"

  # For DEV images enable error displays and
  if ! [ -z ${DEV_ENABLED} ]; then
    # Turn on additional options for PHP and server development.
    config php --multi \
      'display_errors' 'On' \
      'display_startup_errors' 'On' \
      'zend.assertions' '1' \
      'assert.exception' 'On' \
      'max_input_vars' '10000' \
      'max_input_time' '60'

    # On development environments, expect constant code changes.
    config php "opcache.revalidate_freq" "5" "opcache"

    # Install and configure X-Debug for use with the environment.
    apk add --update --no-cache php${PHP_VER}-xdebug
    XDEBUG_INI=$(find $PHP_INI_DIR -name *xdebug.ini)

    if ! [ -z ${XDEBUG_INI} ] && [ -f ${XDEBUG_INI} ]; then
      sed -i -e s/";\\s*\\(zend_extension\\s*=.*\\)$"/"\\1"/ ${XDEBUG_INI}

      config php --file=xdebug --multi \
        'xdebug.mode' 'off' \
        'xdebug.remote_port' '9003' \
        'xdebug.max_nesting_level' '256' \
        'xdebug.client_host' 'host.docker.internal'

      # Disable JIT because it is incompatible with XDebug.
      config php "opcache.jit_buffer_size" "0"
    fi
  fi

  # Configure and install uploadprogress library.
  #
  # - Alpine has created a package for this so we no longer
  # - needed to build this from source, kept snippet here
  # - as a reference to building PHP libs in case it's
  # - needed in the future.
  # -----------------------------------------------
  #apk add --update --no-cache --virtual .build-deps \
  #  git php${PHP_VER}-dev pcre-dev build-base autoconf libtool re2c

  #git clone https://github.com/Jan-E/uploadprogress.git
  #cd uploadprogress && \
  #phpize && \
  #./configure && \
  #make install && \
  #rm -rf uploadprogress && \
  #echo "extension=uploadprogress.so" >> "$PHP_INI_DIR/php.ini"

  #apk del .build-deps && rm -rf .build-deps
  # END BUILD ------------------------------------
}

function setup_drush_launcher {
  wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/latest/download/drush.phar
  chmod +x drush.phar
  mv drush.phar /usr/local/bin/drush
}

#
# Add the need configurations and enable the Apache modules.
#
function setup_apache {
  rm -rf "/var/www/localhost"
  mkdir -p "/run/apache2"

  # Create the directory for keeping the various VHosts configurations.
  mkdir -p /etc/apache2/vhost.d

  # Enable the necessary Apache modules.
  apachemod en headers_module
  apachemod en expires_module
  apachemod en mime_module
  apachemod en deflate_module
  apachemod en rewrite_module

  # Disable Apache modules that aren't needed.
  apachemod ds proxy_module

  if ! [ -z ${DEV_ENABLED} ]; then
    # Create a URI to reset the Apache PHP APCu and OpCache.
    # Needs to be reset from a PHP worker thread.
    vhost create "_sys/reset" "reset.localhost"
    echo "127.0.0.1\t reset.localhost" >> /etc/hosts
    mkdir -p "/var/www/html/_sys/reset"
    echo "<?php print (opcache_reset() && apcu_clear_cache()) ? 'Complete' : 'Failed';" > "/var/www/html/_sys/reset/index.php"
  fi

  # Allow s6 to start the SSH daemon
  cp -a /root/s6/apache2/. /etc/s6/apache2
}

#
# Install and setup SSHD daemon. Should be run on each individual environments
# so that SSHD will have unique SSH keys and signatures for each instance.
#
function setup_sshd {
  apk add --update --no-cache openssh

  # Generate and configure the SSH keys.
  ssh-keygen -q -t dsa -f /etc/ssh/ssh_host_dsa_key
  ssh-keygen -q -t ed25519 -f /etc/ssh/ssh_host_ed25519_key
  ssh-keygen -q -b 2048 -t rsa -f /etc/ssh/ssh_host_rsa_key
  ssh-keygen -q -b 521 -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key

  chmod 0200 /etc/ssh/ssh_host_dsa_key
  chmod 0200 /etc/ssh/ssh_host_ed25519_key
  chmod 0200 /etc/ssh/ssh_host_rsa_key
  chmod 0200 /etc/ssh/ssh_host_ecdsa_key

  # Ensure password authentication is disabled and SSH Key auth is allowed.
  config sshd PasswordAuthentication no
  config sshd PermitEmptyPasswords no
  config sshd PubkeyAuthentication yes

  # Allow s6 to start the SSH daemon
  cp -a /root/s6/sshd/. /etc/s6/sshd
}

#
# Install and setup the Redis server.
#
function setup_redis {
  apk add --update --no-cache redis

  # Alter the Redis server configurations.
  config redis --multi \
    "daemonize" "no" \
    "pidfile" "/run/redis.pid" \
    "loglevel" "warning" \
    "logfile" "/dev/stderr" \
    "maxmemory" "64mb" \
    "maxmemory-policy" "volatile-lru"

  # Allow s6 to start the Redis server
  cp -a /root/s6/redis/ /etc/s6/redis
}

#
# Install and setup the memcached server.
#
function setup_memcached {
  apk add --update --no-cache memcached

  adduser -s /bin/bash -S -g memcached memcached

  # Allow s6 to start the Memcached server.
  cp -a /root/s6/memcached/ /etc/s6/memcached
}

#
# Install and setup the CRON service.
#
function setup_cron {
  apk add --update --no-cache dcron

  # Allow s6 to start the CRON services.
  cp -a /root/s6/cron/ /etc/s6/cron
}

#
# Defer any actual work of the script until after all the functions
# and commands have been defined.
#
main "$@"
